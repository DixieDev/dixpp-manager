#pragma once
#include <gtkmm/application.h>
#include "../include/DixppMainWindow.hpp"

class App : public Gtk::Application{
	private:
		void createWindow();
		void closeWindowOnHide();
	protected:
		App();
		void on_activate() override;
		void on_startup() override;

		DixppMainWindow* m_window;
	public:
		virtual ~App() = default;
		static Glib::RefPtr<App> create();
};
