#pragma once
#include <gtkmm/dialog.h>
#include <gtkmm/entry.h>
#include <string>

class TextInputDialog : public Gtk::Dialog{
	private:
		std::string text;
		Gtk::Entry entry;
	public:
		TextInputDialog(const char* _title = "", const char* _defText = nullptr);
		std::string getText();
};
