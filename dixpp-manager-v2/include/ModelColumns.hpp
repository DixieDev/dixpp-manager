#pragma once
#include <gtkmm/treemodelcolumn.h>

enum class ListingType{
	FILE = 1,
	FILTER = 2
};

class ModelColumns : public Gtk::TreeModelColumnRecord{
	public:
		ModelColumns();
		Gtk::TreeModelColumn<ListingType> m_columnType;
		Gtk::TreeModelColumn<Glib::ustring> m_columnText;
};

extern ModelColumns CustomColumns;
