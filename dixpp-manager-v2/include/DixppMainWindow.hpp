#pragma once
#include <gtkmm/applicationwindow.h>
#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include <gtkmm/notebook.h>
#include <gtkmm/label.h>
#include <gtkmm/treeview.h>
#include <gtkmm/treestore.h>
#include <gtkmm/actiongroup.h>
#include <gtkmm/menubar.h>
#include <gtkmm/scrolledwindow.h>
#include "../include/ModelColumns.hpp"

class DixppProject;

class DixppMainWindow : public Gtk::ApplicationWindow{
	private:
		DixppProject* m_project;
		Gtk::VBox m_vboxRoot;

		Gtk::MenuBar* m_menuBar;

		Gtk::HBox m_hboxBuildButtons;
		Gtk::Button m_buttonBuild;
		Gtk::Button m_buttonExecute;
		Gtk::Button m_buttonBuildAndExecute;

		Gtk::Notebook m_nbProjectContents;

		Gtk::VBox m_vboxFiles;
		Gtk::ScrolledWindow m_scrollingFiles;
		Gtk::TreeView m_treeFilesView;
		Glib::RefPtr<Gtk::TreeStore> m_treeFiles;
		Gtk::TreeStore::Row m_sourceRow;
		Gtk::TreeStore::Row m_headerRow;
		Gtk::HBox m_hboxFileButtons;
		Gtk::Button m_buttonAddSource;
		Gtk::Button m_buttonAddHeader;
		Gtk::Button m_buttonRemoveFile;
		
		Gtk::VBox m_vboxFlags;
		Gtk::ScrolledWindow m_scrollingFlags;
		Gtk::TreeView m_treeFlagsView;
		Glib::RefPtr<Gtk::TreeStore> m_treeFlags;
		Gtk::HBox m_hboxFlagButtons;
		Gtk::Button m_buttonAddFlag;
		Gtk::Button m_buttonEditFlag;
		Gtk::Button m_buttonRemoveFlag;
	
		Gtk::Label m_labelNotebookSettings;

	public:
		DixppMainWindow();
		virtual ~DixppMainWindow();

		/** Closes the currently open project, unless the user opts not to in
		 * the case that they have unsaved changes.
		 * Returns true if the project is closed, otherwise returns false */
		bool closeProject();

		/** Creates a new Dixpp Project with the specified name in the specified
		 * directory */
		void makeNewProject(const std::string& _name, const std::string& _dir);

		/** Opens a new Dixpp Project and returns true on success. Returns false
		 * if the user decides against opening another project over their current
		 * one, or if the opening process fails for whatever reason. */
		bool openProject(const std::string& _filename);

		/** Saves the project if changes have been made to it and it's actually
		 * open. */
		void saveProject();

		/** Adds a source file to the currently open project */
		void addSourceFileToProject(const std::string& _filename);

		/** Adds a header file to the currently open project */
		void addHeaderFileToProject(const std::string& _filename);
	
		/** Removes a file from the project **/
		void removeFileFromProject(Gtk::TreeModel::iterator _iter);

		/** Adds a compiler flag to the currently open project */
		void addFlagToProject(const std::string& _flag);
	
		/** Removes a flag from the project **/
		void removeFlagFromProject(Gtk::TreeModel::iterator _iter);

		//--------------------------  CALLBACKS  --------------------------//
		void onOpenAction();
		void onSaveAction();
		void onNewAction();
		void onBuildClicked();
		void onExecuteClicked();
		void onBuildAndExecuteClicked();
		void onAddSourceClicked();
		void onAddHeaderClicked();
		void onRemoveFileClicked();
		void onAddFlagClicked();
		void onEditFlagClicked();
		void onRemoveFlagClicked();
};
