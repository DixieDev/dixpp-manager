#pragma once
#include <string>
#include <vector>

class DixppProject{
private:
	/* Just the project's name */
	std::string m_projectName;

	/* The directory of the project */
	std::string m_projectDir;

	/* A list of source files the project will compile */
	std::vector<std::string> m_srcFiles;

	/* A list of header files used in the project */
	std::vector<std::string> m_headerFiles;

	/* A list of compiler flags */
	std::vector<std::string> m_flags;

	/* Dictates whether or not xterm should be automatically opened when the application
	 * is executed */
	bool m_openTerminal;

	/* True when there are unsaved changes to the project, otherwise false */
	bool m_changed;

	/* True if the project can be safely operated on, otherwise false */
	bool m_ready;

	/* Replaces all backslashes with forward slashes in the provided string */
	void standardisePath(std::string& _path);
public:
	/* Creates a new project with the specified name in the specified directory */
	DixppProject(const std::string& _projectName, const std::string& _projectDirectory);

	/* Loads a project with the specified filename */
	DixppProject(const std::string& _filename);

	/* Default destructor */
	~DixppProject();

	/* Loads a project with the specified filename.
	 * Returns true on success, and false otherwise */
	bool load(const std::string& _filename);

	/* Saves any changes to the project */
	void save();

	/* Adds the specified resource based on its command prefix. Prefixes:
	 * * ^  -   Header file
	 * * $  -   Source file
	 * * !  -   Compiler flag
	 * Returns true on success, and false otherwise */
	bool addResource(const std::string& _command);

	/* Adds and, if necessary, creates a source file in the project directory with the
	 * specified filename */
	void addSourceFile(const std::string& _filename);

	/* Removes the source file at the specified index */
	void removeSourceFile(unsigned int _index);

	/* Adds and, if necessary, creates a header file in the project directory with the
	 * specified filename */
	void addHeaderFile(const std::string& _filename);

	/* Removes the header file at the specified index */
	void removeHeaderFile(unsigned int _index);

	/* Adds a flag to be used by the compiler */
	void addFlag(const std::string& _flag);

	/* Removes the flag at the specified index */
	void removeFlag(unsigned int _index);

	/* Compiles the project */
	void compile();

	/* Executes the project, compiling if necessary */
	void execute();

	/* Specify whether or not to automatically open the project in a new xterm window */
	void enableTerminal(bool _enabled = true);

	//----------------------------  CONST  ----------------------------//

	/* Returns the current project directory */
	const std::string& getProjectDirectory() const;

	/* Returns the index of a source file with the matching filename.
	 * If the specified file does not exist, the source file list's
	 * size is returned instead. */
	unsigned int getSourceFileIndex(const std::string& _filename) const;
	
	/* Returns the source filename at the specified index */
	const std::string& getSourceFile(unsigned int _index) const;

	/* Returns the total number of source files added to the project */
	unsigned int getSourceFileCount() const;

	/* Returns the index of a header file with the matching filename 
	 * If the specified file does not exist, the header file list's
	 * size is returned instead. */
	unsigned int getHeaderFileIndex(const std::string& _filename) const;
	
	/* Returns the header filename at the specified index */
	const std::string& getHeaderFile(unsigned int _index) const;

	/* Returns the total number of header files added to the project */
	unsigned int getHeaderFileCount() const;

	/* Returns the index of a matching flag. If the specified flag does
	 * not exist, the flag list's size is returned instead. */
	unsigned int getFlagIndex(const std::string& _flag) const;
	
	/* Returns the flag at the specified index */
	const std::string& getFlag(unsigned int _index) const;

	/* Returns the total number of flags added to the compiler flag list */
	unsigned int getFlagCount() const;

	/* Returns true if the program will launch in the terminal on execution,
	 * otherwise returns false */
	bool isTerminalEnabled() const;

	/* Returns true if there are unsaved changes to the project, otherwise
	 * returns false */
	bool hasUnsavedChanges() const;

	/* Returns true if the project is ready to be used, and false otherwise */
	bool isReady() const;
};
