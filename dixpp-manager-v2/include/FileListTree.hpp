#pragma once
#include <gtkmm/treestore.h>

class FileListTree : public Gtk::TreeStore{
	public:
		FileListTree() = default;
		~FileListTree() = default;
};
