#include "../include/DixppProject.hpp"
#include <cstdlib>
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <unistd.h>

DixppProject::DixppProject(const std::string& _projectName, const std::string& _projectDirectory){
	m_projectName = _projectName;
	m_projectDir = _projectDirectory;
	standardisePath(m_projectDir);
	while(m_projectDir.at(m_projectDir.size()-1) == '/'){
		  m_projectDir = m_projectDir.substr(0, m_projectDir.size() - 1);
	}
	m_openTerminal = true;
	m_changed = true;
	m_ready = true;
}

DixppProject::DixppProject(const std::string& _filename){
	std::string filename = _filename;
	standardisePath(filename);
	m_projectName = "";
	m_projectDir = "";
	m_openTerminal = true;
	if(load(filename)){
		m_ready = true;
	}
	else{
		m_ready = false;
	}
}

DixppProject::~DixppProject(){
	//No necessary cleanup yet but there might be eventually
}

void DixppProject::standardisePath(std::string& _path){
	for(size_t pos = _path.find("\\"); pos != std::string::npos; pos = _path.find("\\")){
		_path.at(pos) = '/';
	}
}

bool DixppProject::load(const std::string& _filename){
	std::string filename = _filename;
	standardisePath(filename);
	std::ifstream reader;
	reader.open(filename);
	if(reader.is_open()){
		m_projectName = "";
		size_t lastSlashLoc = filename.find_last_of('/');
		m_projectDir = filename.substr(0, lastSlashLoc);
		m_openTerminal = true;
		m_srcFiles.clear();
		m_headerFiles.clear();
		m_flags.clear();
		std::string curLine = "";
		if(!reader.good()){
			std::cout << filename << " could not be loaded as the file is empty\n";
			reader.close();
			return false;
		}
		std::getline(reader, curLine);
		if(curLine != "dixpp-v1.0"){
			std::cout << filename << " could not be loaded as it is not a "
					  << "Dixpp Project file\n";
			reader.close();
			return false;
		}
		std::getline(reader, curLine);
		m_projectName = curLine;
		std::getline(reader, curLine);
		m_openTerminal = (bool)atoi(curLine.c_str());
		std::cout.flush();
		while(reader.good()){
			std::getline(reader, curLine);
			addResource(curLine);
		}
		reader.close();
	}
	else{
		std::cout << "The project file '" << filename << "' could not be accessed\n";
		return false;
	}
	m_changed = false;
	return true;
}

void DixppProject::save(){
	std::ofstream writer;
	writer.open((m_projectDir + "/" + m_projectName + ".dpp").c_str());
	writer << "dixpp-v1.0\n";
	writer << m_projectName << "\n";
	writer << (int) m_openTerminal << "\n";
	for(auto& flag : m_flags){
		writer << "!" << flag << "\n";
	}
	for(auto& src : m_srcFiles){
		writer << "$" << src << "\n";
	}
	for(auto& header : m_headerFiles){
		writer << "^" << header << "\n";
	}
	writer.close();
	m_changed = false;
}

bool DixppProject::addResource(const std::string& _command){
	if(_command.size()>1){
		std::string body = _command;
		body.erase(body.begin());
		switch(_command.at(0)){
			case '$':{  //Source file
				addSourceFile(body);
			} break;
			case '^':{  //Header file
				addHeaderFile(body);
			} break;
			case '!':{  //Flags
				addFlag(body);
			} break;
			default:{
				return false;
			} break;
		}
		return true;
	}
	return false;
}

void DixppProject::addSourceFile(const std::string& _filename){
	std::string filename =  _filename;
	standardisePath(filename);
	//remove leading m_projectDir from filename
	size_t pos = filename.find(m_projectDir);
	if(pos != std::string::npos){
		filename = filename.substr(pos+m_projectDir.size()+1);
	}
	std::ofstream writer;
	std::ifstream reader;
	std::string fullFilename = m_projectDir + "/" + filename;
	reader.open(fullFilename.c_str());
	if(reader.is_open()){
		m_srcFiles.push_back(filename);
		m_changed = true;
		reader.close();
		return;
	}
	writer.open(fullFilename.c_str());
	if(writer.is_open()){
		writer << "// '" << filename << "'\n";
		writer.close();
	}
	else{
		std::cout << "The source file '" << fullFilename << "' could not be accessed\n";
		return;
	}
	m_srcFiles.push_back(filename);
	m_changed = true;
}

void DixppProject::removeSourceFile(unsigned int _index){
	if(_index < m_srcFiles.size())
		m_srcFiles.erase(m_srcFiles.begin() + _index);
	m_changed = true;
}

void DixppProject::addHeaderFile(const std::string& _filename){
	std::string filename = _filename;
	standardisePath(filename);
	//remove leading m_projectDir from filename
	size_t pos = filename.find(m_projectDir);
	if(pos != std::string::npos){
		filename = filename.substr(pos+m_projectDir.size()+1);
	}
	std::ofstream writer;
	std::ifstream reader;
	std::string fullFilename = m_projectDir + "/" + filename;
	reader.open(fullFilename.c_str());
	if(reader.is_open()){
		m_headerFiles.push_back(filename);
		m_changed = true;
		reader.close();
		return;
	}
	writer.open(fullFilename.c_str());
	if(writer.is_open()){
		writer << "#pragma once // '" << filename << "'\n";
		writer.close();
	}
	else{
		std::cout << "The header file '" << fullFilename << "' could not be accessed\n";
		return;
	}
	m_headerFiles.push_back(filename);
	m_changed = true;
}

void DixppProject::removeHeaderFile(unsigned int _index){
	if(_index < m_headerFiles.size())
		m_headerFiles.erase(m_headerFiles.begin() + _index);
	m_changed = true;
}

void DixppProject::addFlag(const std::string& _flag){
	m_flags.push_back(_flag);
	m_changed = true;
}

void DixppProject::removeFlag(unsigned int _index){
	if(_index < m_flags.size())
		m_flags.erase(m_flags.begin() + _index);
	m_changed = true;
}

void DixppProject::compile(){
	//compile the program
	std::string cmd = "g++ ";
	for(auto& src : m_srcFiles){
		cmd += m_projectDir + "/" + src + " ";
	}
	for(auto& flag : m_flags){
		cmd += flag + " ";
	}
	cmd += "-o" + m_projectDir + "/" + m_projectName + ".out";
	cmd = cmd;
	std::cout << "Building with '" + cmd + "'\n";
	FILE* terminalPipe = popen(cmd.c_str(), "r");
	/*FILE* terminalPipe = popen("pkg-config --cflags gtkmm-3.0", "r"); //test
	char buf[1000];
	std::string out = "";
    fgets(buf, 1000, terminalPipe);
    if(buf){
        std::cout << buf << "\n";
    }*/
	pclose(terminalPipe);
}

void DixppProject::execute(){
	//execute the program (compiling if necessary)
	std::ifstream reader;
	reader.open((m_projectDir + "/" + m_projectName + ".out").c_str());
	if(!reader.is_open()){
		compile();
		reader.open((m_projectDir + "/" + m_projectName + ".out").c_str());
		if(!reader.is_open()){
			std::cout << "Failed to access " << m_projectDir << "/" << m_projectName << ".out\n";
			return;
		}
	}
	reader.close();
	std::string cmd = m_projectDir + "/" + m_projectName + ".out";
	if(m_openTerminal){
		cmd = "xterm " + cmd;
		std::cout << "Opening xterm\n";
	}
	std::cout << "Executing '" + cmd + "'\n";
	FILE* applicationPipe = popen(cmd.c_str(), "r");
	pclose(applicationPipe);
}

void DixppProject::enableTerminal(bool _enabled){
	m_openTerminal = _enabled;
}

const std::string& DixppProject::getProjectDirectory() const{
	return m_projectDir;
}

unsigned int DixppProject::getSourceFileIndex(const std::string& _filename) const{
	for(int i=0; i<m_srcFiles.size(); i++){
		if(m_srcFiles.at(i) == _filename)
			return i;
	}
	return m_srcFiles.size();
}

const std::string& DixppProject::getSourceFile(unsigned int _index) const{
	if(_index < m_srcFiles.size())
		return m_srcFiles.at(_index);
	return "";
}

unsigned int DixppProject::getSourceFileCount() const{
	return m_srcFiles.size();
}

unsigned int DixppProject::getHeaderFileIndex(const std::string& _filename) const{
	for(int i=0; i<m_headerFiles.size(); i++)
	{
		if(m_headerFiles.at(i) == _filename)
		   return i;
	}
	return m_headerFiles.size();
}

const std::string& DixppProject::getHeaderFile(unsigned int _index) const{
	if(_index < m_headerFiles.size())
		return m_headerFiles.at(_index);
	return "";
}

unsigned int DixppProject::getHeaderFileCount() const{
	return m_headerFiles.size();
}

unsigned int DixppProject::getFlagIndex(const std::string& _flag) const{
	for(int i=0; i<m_flags.size(); i++){
		if(m_flags.at(i) == _flag)
			return i;
	}
	return m_flags.size();
}

const std::string& DixppProject::getFlag(unsigned int _index) const{
	if(_index < m_flags.size())
		return m_flags.at(_index);
	return "";
}

unsigned int DixppProject::getFlagCount() const{
	return m_flags.size();
}

bool DixppProject::isTerminalEnabled() const{
	return m_openTerminal;
}

bool DixppProject::hasUnsavedChanges() const{
	return m_changed;
}

bool DixppProject::isReady() const{
	return m_ready;
}
