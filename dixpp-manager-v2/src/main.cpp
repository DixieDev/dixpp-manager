#include <iostream>
#include "../include/App.hpp"
/** TODO:
 * * Test saving and loading    * Done
 * * GUI                        * Ongoing
 * * * Learn to use GTK a bit   * Ongoing
 * * * Get a tree of files goin * Ongoing
 * * * Add callbacks to win     * Ongoing - High priority
 */

int main(int argc, char *argv[])
{
	auto app = App::create();
	const int status = app->run(argc, argv);
	return status;
}
