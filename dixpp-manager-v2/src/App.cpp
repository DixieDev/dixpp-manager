#include "../include/App.hpp"
#include <gtkmm/menubar.h>
#include <gtkmm/menu.h>

Glib::RefPtr<App> App::create(){
	return Glib::RefPtr<App>(new App);
}

App::App(){
}

void App::on_activate(){
	Gtk::Application::on_activate();

	m_window = new DixppMainWindow;
	add_window(*m_window);
	m_window->signal_hide().connect(sigc::mem_fun(*this, &App::closeWindowOnHide));
	m_window->show();
}

void App::on_startup(){
	Gtk::Application::on_startup();

	auto menuBar = Gio::Menu::create();
	auto fileSubMenu = Gio::Menu::create();
	fileSubMenu->append("New", "win.new");
	fileSubMenu->append("Open", "win.open");
	fileSubMenu->append("Save", "win.save");
	menuBar->append_submenu("File", fileSubMenu);
	set_menubar(menuBar);
}

void App::closeWindowOnHide(){
	delete m_window;
}
