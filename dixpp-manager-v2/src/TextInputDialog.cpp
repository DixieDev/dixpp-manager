#include "../include/TextInputDialog.hpp"
#include <gtkmm/label.h>


TextInputDialog::TextInputDialog(const char* _title, const char* _defText){
	set_title(_title);
	entry.set_size_request(200, 50);
	if(_defText){
		entry.set_text(_defText);
	}
	else{
		entry.set_text("hello");
	}
	entry.select_region(0, entry.get_text_length());
	get_vbox()->pack_start(entry);
	show_all_children();
}

std::string TextInputDialog::getText(){
	std::string output = "";
	if(entry.get_text_length() != 0){
		output = entry.get_text().raw();
	}
	return output;
}
