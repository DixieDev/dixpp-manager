#include "../include/DixppMainWindow.hpp"
#include <iostream>
#include <string>
#include <gtkmm/stock.h>
#include <gtkmm/action.h>
#include <gtkmm/filechooserdialog.h>
#include <gdkmm/event.h>
#include "../include/DixppProject.hpp"
#include "../include/TextInputDialog.hpp"

DixppMainWindow::DixppMainWindow(){
	m_project = nullptr;

	set_size_request(300, 600);
	set_border_width(10);

	//Setup menu bar actions
	add_action("new", sigc::mem_fun(*this, &DixppMainWindow::onNewAction));
	add_action("open", sigc::mem_fun(*this, &DixppMainWindow::onOpenAction));
	add_action("save", sigc::mem_fun(*this, &DixppMainWindow::onSaveAction));

	/* Set up layout
	 * Blocks used for clarity? I hope it works out that way */
	{
		add(m_vboxRoot);
		{
			m_vboxRoot.pack_start(m_hboxBuildButtons, Gtk::PackOptions::PACK_SHRINK);
			{
				m_hboxBuildButtons.pack_start(m_buttonBuild);
				{
					m_buttonBuild.set_label("Build");
					m_buttonBuild.set_size_request(100, 30);
					m_buttonBuild.set_tooltip_text("Build the project into an executable file");
					m_buttonBuild.signal_clicked().connect(sigc::mem_fun(*this, &DixppMainWindow::onBuildClicked));
				}
				m_hboxBuildButtons.pack_start(m_buttonExecute);
				{
					m_buttonExecute.set_label("Execute");
					m_buttonExecute.set_size_request(100, 30);
					m_buttonExecute.set_tooltip_text("Execute the built file, building one if it doesn't already exist");
					m_buttonExecute.signal_clicked().connect(sigc::mem_fun(*this, &DixppMainWindow::onExecuteClicked));
				}
				m_hboxBuildButtons.pack_start(m_buttonBuildAndExecute);
				{
					m_buttonBuildAndExecute.set_label("Bld + Exe");
					m_buttonBuildAndExecute.set_size_request(100, 30);
					m_buttonBuildAndExecute.set_tooltip_text("Build and execute the project");
					m_buttonBuildAndExecute.signal_clicked().connect(sigc::mem_fun(*this, &DixppMainWindow::onBuildAndExecuteClicked));
				}
			}

			m_vboxRoot.pack_start(m_nbProjectContents);
			{
				m_labelNotebookSettings.set_label("Project settings go here");
				m_nbProjectContents.set_size_request(300);
				m_nbProjectContents.set_border_width(5);
				m_nbProjectContents.append_page(m_vboxFiles, "Files");
				{
					m_vboxFiles.pack_start(m_scrollingFiles);
					{
						m_scrollingFiles.add(m_treeFilesView);
						{
							m_treeFiles = Gtk::TreeStore::create(CustomColumns);
							m_treeFilesView.set_model(m_treeFiles);
							m_headerRow = *(m_treeFiles->append());
							m_headerRow[CustomColumns.m_columnType] = ListingType::FILTER;
							m_headerRow[CustomColumns.m_columnText] = "Headers";
							m_sourceRow = *(m_treeFiles->append());
							m_sourceRow[CustomColumns.m_columnType] = ListingType::FILTER;
							m_sourceRow[CustomColumns.m_columnText] = "Sources";
							m_treeFilesView.append_column("Filename", CustomColumns.m_columnText);
							m_treeFilesView.get_column(0)->set_sort_column(CustomColumns.m_columnText);
						}
					}
					m_vboxFiles.pack_start(m_hboxFileButtons, Gtk::PackOptions::PACK_SHRINK);
					{
						m_hboxFileButtons.pack_start(m_buttonAddSource);
						{
							m_buttonAddSource.set_label("+Source");
							m_buttonAddSource.set_size_request(100, 30);
							m_buttonAddSource.set_tooltip_text("Add a source file to the project.");
							m_buttonAddSource.signal_clicked().connect(sigc::mem_fun(*this, &DixppMainWindow::onAddSourceClicked));
						}
						m_hboxFileButtons.pack_start(m_buttonAddHeader);
						{
							m_buttonAddHeader.set_label("+Header");
							m_buttonAddHeader.set_size_request(100, 30);
							m_buttonAddHeader.set_tooltip_text("Add a header file to the project.");
							m_buttonAddHeader.signal_clicked().connect(sigc::mem_fun(*this, &DixppMainWindow::onAddHeaderClicked));
						}
						m_hboxFileButtons.pack_start(m_buttonRemoveFile);
						{
							m_buttonRemoveFile.set_label("-Remove");
							m_buttonRemoveFile.set_size_request(100, 30);
							m_buttonRemoveFile.set_tooltip_text("Remove the currently selected entry.");
							m_buttonRemoveFile.signal_clicked().connect(sigc::mem_fun(*this, &DixppMainWindow::onRemoveFileClicked));
						}
					}
				}
				m_nbProjectContents.append_page(m_vboxFlags, "Flags");
				{
					m_vboxFlags.pack_start(m_scrollingFlags);
					{
						m_scrollingFlags.add(m_treeFlagsView);
						{
							m_treeFlags = Gtk::TreeStore::create(CustomColumns);
							m_treeFlagsView.set_model(m_treeFlags);
							m_treeFlagsView.append_column("Flag", CustomColumns.m_columnText);
						}
					}
					m_vboxFlags.pack_start(m_hboxFlagButtons, Gtk::PackOptions::PACK_SHRINK);
					{
						m_hboxFlagButtons.pack_start(m_buttonAddFlag);
						{
							m_buttonAddFlag.set_label("+Flag");
							m_buttonAddFlag.set_size_request(100, 30);
							m_buttonAddFlag.set_tooltip_text("Add a compiler or linker argument/option.");
							m_buttonAddFlag.signal_clicked().connect(sigc::mem_fun(*this, &DixppMainWindow::onAddFlagClicked));
						}
						m_hboxFlagButtons.pack_start(m_buttonEditFlag);
						{
							m_buttonEditFlag.set_label("+Edit");
							m_buttonEditFlag.set_size_request(100, 30);
							m_buttonEditFlag.set_tooltip_text("Edit the currently selected entry.");
							m_buttonEditFlag.signal_clicked().connect(sigc::mem_fun(*this, &DixppMainWindow::onEditFlagClicked));
						}
						m_hboxFlagButtons.pack_start(m_buttonRemoveFlag);
						{
							m_buttonRemoveFlag.set_label("-Remove");
							m_buttonRemoveFlag.set_size_request(100, 30);
							m_buttonRemoveFlag.set_tooltip_text("Remove the currently selected entry.");
							m_buttonRemoveFlag.signal_clicked().connect(sigc::mem_fun(*this, &DixppMainWindow::onRemoveFlagClicked));
						}
					}
				}
				m_nbProjectContents.append_page(m_labelNotebookSettings, "Settings");

			}
		}
	}
	//m_buttonTest
	//    .signal_clicked()
	//    .connect(sigc::mem_fun(*this, &DixppMainWindow::onButtonTestClicked));

	show_all_children();
}

DixppMainWindow::~DixppMainWindow(){
	std::cout << "RIP\n";
}

bool DixppMainWindow::closeProject(){
	if(m_project){
		if(m_project->hasUnsavedChanges()){
			//ask user if they wish to save, not save, or cancel the closure
		}
		delete m_project;
		m_project = nullptr;
		for(auto iter = m_headerRow->children().begin(); iter = m_headerRow->children().begin(); iter != m_headerRow->children().end()){
			m_treeFiles->erase(iter);
		}
		for(auto iter = m_sourceRow->children().begin(); iter = m_sourceRow->children().begin(); iter != m_sourceRow->children().end()){
			m_treeFiles->erase(iter);
		}
		m_treeFlags->clear();
		return true;
	}
	return true;
}

void DixppMainWindow::makeNewProject(const std::string& _name,
									 const std::string& _dir){
	if(closeProject()){
 	   m_project = new DixppProject(_name, _dir);
	}
}

bool DixppMainWindow::openProject(const std::string& _filename){
	if(closeProject()){
		m_project = new DixppProject(_filename);
		if(m_project->isReady()){
			//add source files to corresponding notebook and header
			for(int i=0; i<m_project->getSourceFileCount(); i++){
				const std::string& file = m_project->getSourceFile(i);
				if(file != ""){
					Gtk::TreeModel::iterator iter = m_treeFiles->append(m_sourceRow.children());
					Gtk::TreeModel::Row row = *iter;
					row[CustomColumns.m_columnType] = ListingType::FILE;
					row[CustomColumns.m_columnText] = file;
				}
			}
			//add header files to corresponding notebook and header
			for(int i=0; i<m_project->getHeaderFileCount(); i++){
				const std::string& file = m_project->getHeaderFile(i);
				if(file != ""){
					Gtk::TreeModel::iterator iter = m_treeFiles->append(m_headerRow.children());
					Gtk::TreeModel::Row row = *iter;
					row[CustomColumns.m_columnType] = ListingType::FILE;
					row[CustomColumns.m_columnText] = file;
				}
			}
			//add flag list to corresponding notebook
			for(int i=0; i<m_project->getFlagCount(); i++){
				const std::string& flag = m_project->getFlag(i);
				Gtk::TreeModel::iterator iter = m_treeFlags->append();
				Gtk::TreeModel::Row row = *iter;
				row[CustomColumns.m_columnType] = ListingType::FILE;
				row[CustomColumns.m_columnText] = flag;
			}
			return true;
		}
	}
	return false;
}

void DixppMainWindow::saveProject(){
	if(m_project && m_project->hasUnsavedChanges()){
		std::cout << "Saving\n";
		m_project->save();
	}
}

void DixppMainWindow::addSourceFileToProject(const std::string& _filename){
	if(m_project){
		m_project->addSourceFile(_filename);
		const std::string& newFile = m_project->getSourceFile(m_project->getSourceFileCount()-1);
		if(newFile != ""){
			//add to source file list in notebook
			Gtk::TreeModel::iterator iter = m_treeFiles->append(m_sourceRow.children());
			Gtk::TreeModel::Row row = *iter;
			row[CustomColumns.m_columnType] = ListingType::FILE;
			row[CustomColumns.m_columnText] = newFile;
		}
	}
}

void DixppMainWindow::removeFileFromProject(Gtk::TreeModel::iterator _iter){
	if(m_project){
		Gtk::TreeModel::Row row = *_iter;
		Glib::ustring filename = row[CustomColumns.m_columnText];
		auto parent = row.parent();
		if(!parent){
			std::cout << "Can't remove default filters\n";	
			return;
		}
		std::cout << "Attempting to remove '" << filename << "'.\n";
		Gtk::TreeModel::Row parentRow = *parent;
		if(parentRow == m_headerRow){
			std::cout << "Identified as header file\n";	
			unsigned int fileIndex = m_project->getHeaderFileIndex(filename.raw());
			if(fileIndex != m_project->getHeaderFileCount()){
					m_project->removeHeaderFile(fileIndex);
					m_treeFiles->erase(_iter);
			}
		}
		else if(parentRow == m_sourceRow){
			std::cout << "Identified as source file\n";	
			unsigned int fileIndex = m_project->getSourceFileIndex(filename.raw());
			if(fileIndex != m_project->getSourceFileCount()){
					m_project->removeSourceFile(fileIndex);
					m_treeFiles->erase(_iter);
			}
		}
	}
}

void DixppMainWindow::addHeaderFileToProject(const std::string& _filename){
	if(m_project){
		m_project->addHeaderFile(_filename);
		const std::string& newFile = m_project->getHeaderFile(m_project->getHeaderFileCount()-1);
		if(newFile != ""){
			//add to header file list in notebook
			Gtk::TreeModel::iterator iter = m_treeFiles->append(m_headerRow.children());
			Gtk::TreeModel::Row row = *iter;
			row[CustomColumns.m_columnType] = ListingType::FILE;
			row[CustomColumns.m_columnText] = newFile;
		}
	}
}

void DixppMainWindow::addFlagToProject(const std::string& _flag){
	if(m_project){
		m_project->addFlag(_flag);
		Gtk::TreeModel::iterator iter = m_treeFlags->append();
		Gtk::TreeModel::Row row = *iter;
		row[CustomColumns.m_columnType] = ListingType::FILE;
		row[CustomColumns.m_columnText] = _flag;
	}
}

void DixppMainWindow::removeFlagFromProject(Gtk::TreeModel::iterator _iter){
	if(m_project){
		Gtk::TreeModel::Row row = *_iter;
		Glib::ustring flag = row[CustomColumns.m_columnText];
		unsigned int flagIndex = m_project->getFlagIndex(flag.raw());
		if(flagIndex != m_project->getFlagCount()){
			m_project->removeFlag(flagIndex);
			m_treeFlags->erase(_iter);
		}
	}
}


void DixppMainWindow::onNewAction(){
	Gtk::FileChooserDialog chooser("Choose a name and directory for your project", Gtk::FILE_CHOOSER_ACTION_SAVE);
	chooser.set_transient_for(*this);
	chooser.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
	chooser.add_button("Create", Gtk::RESPONSE_OK);

	if(m_project){
		chooser.set_current_folder(m_project->getProjectDirectory());
	}

	int result = chooser.run();
	switch(result){
		case Gtk::RESPONSE_OK:{
			std::cout << "Attempting to create '" << chooser.get_filename() << "'...\n";
			std::string projectDir = chooser.get_filename();
			size_t lastSlashPos = projectDir.find_last_of("/\\");
			if(lastSlashPos != std::string::npos){
				std::string projectName = projectDir.substr(lastSlashPos+1);
				projectDir = projectDir.substr(0, lastSlashPos);
				if(projectName == ""){
					projectName = "dixproject";
				}
				std::cout << "Making project with the following parameters:\n\t";
				std::cout << "Name: " << projectName << "\n\t";
				std::cout << " Dir: " << projectDir << "\n";
				makeNewProject(projectName, projectDir);
			}
			else{
				std::cout << "Not a fan of the lack of slashes in that filename, something might be wrong so I'm cancelling.\n";
			}
		} break;
		default:{
			std::cout << "Cancelled flag addition operation\n";
		} break;
	}
}

void DixppMainWindow::onOpenAction(){
	Gtk::FileChooserDialog chooser("Choose a project file to open", Gtk::FILE_CHOOSER_ACTION_OPEN);
	chooser.set_transient_for(*this);
	chooser.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
	chooser.add_button("Select", Gtk::RESPONSE_OK);
	if(m_project){
		chooser.set_current_folder(m_project->getProjectDirectory());
	}

	int result = chooser.run();
	switch(result){
		case Gtk::RESPONSE_OK:{
			std::cout << "Attempting to open '" << chooser.get_filename() << "'...\n";
			if(openProject(chooser.get_filename())){
				std::cout << "Success!\n";
			}
			else{
				std::cout << "Cancelled opening process\n";
			}
		} break;
		default:{
			std::cout << "Cancelled flag addition operation\n";
		} break;
	}
}

void DixppMainWindow::onSaveAction(){
	std::cout << "Attempting save...\n";
	saveProject();
}

void DixppMainWindow::onBuildClicked(){
	std::cout << "Build\n";
	if(m_project){
		m_project->compile();
	}
}

void DixppMainWindow::onExecuteClicked(){
	std::cout << "Execute\n";
	if(m_project){
		m_project->execute();
	}
}

void DixppMainWindow::onBuildAndExecuteClicked(){
	std::cout << "Build and Execute\n";
	if(m_project){
		m_project->compile();
		m_project->execute();
	}
}

void DixppMainWindow::onAddSourceClicked(){
	if(!m_project){
		return;
	}
	std::cout << "+src\n";
	Gtk::FileChooserDialog chooser("Choose a source file to add or create", Gtk::FILE_CHOOSER_ACTION_SAVE);
	chooser.set_transient_for(*this);
	chooser.set_current_folder(m_project->getProjectDirectory());
	chooser.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
	chooser.add_button("Ok", Gtk::RESPONSE_OK);

	int result = chooser.run();
	switch(result){
		case Gtk::RESPONSE_OK:{
			std::cout << "Attempting to create '" << chooser.get_filename() << "'...\n";
			std::string filename = chooser.get_filename();
			addSourceFileToProject(filename);
		} break;
		default:{
			std::cout << "Cancelled flag addition operation\n";
		} break;
	}
}

void DixppMainWindow::onAddHeaderClicked(){
	if(!m_project){
		return;
	}
	std::cout << "+hdr\n";
	Gtk::FileChooserDialog chooser("Choose a header file to add or create", Gtk::FILE_CHOOSER_ACTION_SAVE);
	chooser.set_transient_for(*this);
	chooser.set_current_folder(m_project->getProjectDirectory());
	chooser.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
	chooser.add_button("Ok", Gtk::RESPONSE_OK);

	int result = chooser.run();
	switch(result){
		case Gtk::RESPONSE_OK:{
			std::cout << "Attempting to create '" << chooser.get_filename() << "'...\n";
			std::string filename = chooser.get_filename();
			addHeaderFileToProject(filename);
		} break;
		default:{
			std::cout << "Cancelled flag addition operation\n";
		} break;
	}
}

void DixppMainWindow::onRemoveFileClicked(){
	Glib::RefPtr<Gtk::TreeSelection> selection = m_treeFilesView.get_selection();
	if(selection->count_selected_rows() != 0){
		Gtk::TreeModel::iterator iter = selection->get_selected();
		removeFileFromProject(iter);	
	}
	else{
		std::cout << "Select something before you try to delete things\n";	
	}
}

void DixppMainWindow::onAddFlagClicked(){
	if(!m_project){
		return;
	}
	TextInputDialog input("Compiler Flag Entry");
	input.set_transient_for(*this);
	input.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
	input.add_button("Ok", Gtk::RESPONSE_OK);

	int result = input.run();
	if(result == Gtk::RESPONSE_OK){
		std::string text = input.getText();
		if(text.size() > 0){
			addFlagToProject(text);
		}
	}
}

void DixppMainWindow::onEditFlagClicked(){
	Glib::RefPtr<Gtk::TreeSelection> selection = m_treeFlagsView.get_selection();
	if(selection->count_selected_rows() != 0){
		Gtk::TreeModel::iterator iter = selection->get_selected();
		Gtk::TreeModel::Row row = *iter;
		Glib::ustring rowText= row[CustomColumns.m_columnText];
		TextInputDialog input("Compiler Flag Entry", rowText.raw().c_str());
		input.set_transient_for(*this);
		input.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
		input.add_button("Ok", Gtk::RESPONSE_OK);
	
		int result = input.run();
		if(result == Gtk::RESPONSE_OK){
			std::string text = input.getText();
			if(text.size() > 0){
				unsigned int index = m_project->getFlagIndex(rowText.raw());
				if(index != m_project->getFlagCount()){
					row[CustomColumns.m_columnText] = text;
					m_project->removeFlag(index);
					m_project->addFlag(text);
				}
			}
		}
	}
	else{
		std::cout << "Select something before you try to edit things\n";	
	}
}

void DixppMainWindow::onRemoveFlagClicked(){
	if(!m_project){
		return;
	}
	std::cout << "Remove flag clicked\n";
	Glib::RefPtr<Gtk::TreeSelection> selection = m_treeFlagsView.get_selection();
	if(selection->count_selected_rows() != 0){
		Gtk::TreeModel::iterator iter = selection->get_selected();
		removeFlagFromProject(iter);	
	}
	else{
		std::cout << "Select something before you try to delete things\n";	
	}
}
